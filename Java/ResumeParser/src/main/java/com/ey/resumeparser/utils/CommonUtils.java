package com.ey.resumeparser.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

	public static String fetchEmail(String xml) {

		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(xml);
		while (m.find()) {
			return m.group();
		}
		return null;

	}

	public static String fetchPhoneNumber(String xml) {

		Matcher m = Pattern.compile(
				"(?:(?:\\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?")
				.matcher(xml);
		while (m.find()) {
			return m.group();
		}
		return null;

	}

}
