package com.ey.resumeparser.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Div {
	
	@JsonProperty("head")
	private String head;
	
	@JsonProperty("p")
	private String p;
	
	@JsonProperty("xmlns")
	private String xmlns;

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getP() {
		return p;
	}

	public void setP(String p) {
		this.p = p;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

}
