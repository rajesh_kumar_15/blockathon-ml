package com.ey.resumeparser.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Body {

	@JsonProperty("div")
	private List<Div> div;

	@JsonProperty("figure")
	private List<Figure> figure;

	public List<Div> getDiv() {
		return div;
	}

	public void setDiv(List<Div> div) {
		this.div = div;
	}

	public List<Figure> getFigure() {
		return figure;
	}

	public void setFigure(List<Figure> figure) {
		this.figure = figure;
	}
}
