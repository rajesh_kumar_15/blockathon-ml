package com.ey.resumeparser.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResumeData {

	@JsonProperty("body")
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

}
