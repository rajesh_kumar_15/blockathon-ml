package com.ey.resumeparser.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ey.resumeparser.models.Candidate;
import com.ey.resumeparser.service.TekstosenseService;

@RestController
@RequestMapping("/api")
public class ResumePraserController {

	@Autowired
	TekstosenseService tekstosenseService;

	@RequestMapping(value = "/candidate", method = RequestMethod.POST, produces = { "application/json" })
	public List<Candidate> parseResume(@RequestParam("file") MultipartFile file) {
		List<Candidate> candidates = new ArrayList<Candidate>();
		File resume = new File(file.getOriginalFilename());
		try {
			resume.createNewFile();
			FileOutputStream fos;
			fos = new FileOutputStream(resume);
			fos.write(file.getBytes());
			fos.close();
			candidates.add(tekstosenseService.parseResume(resume));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return candidates;
	}

}
