package com.ey.resumeparser.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonIgnoreProperties(ignoreUnknown = true, value = {"fieldTobeIgnored"})
public class Figure {
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("head")
	private String head;
	
	@JsonProperty("label")
	private String label;
	
	@JsonProperty("figDesc")
	private String figDesc;
	
	@JsonProperty("xmlns")
	private String xmlns;
	
	@JsonProperty("xml:id")
	private String xml;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getFigDesc() {
		return figDesc;
	}
	public void setFigDesc(String figDesc) {
		this.figDesc = figDesc;
	}
	public String getXmlns() {
		return xmlns;
	}
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	
	@JsonIgnore 
	public String getXml() {
		return xml;
	}
	@JsonIgnore 
	public void setXml(String xml) {
		this.xml = xml;
	}
	
	

}
