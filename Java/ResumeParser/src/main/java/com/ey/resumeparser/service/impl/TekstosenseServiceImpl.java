package com.ey.resumeparser.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ey.resumeparser.models.Candidate;
import com.ey.resumeparser.service.TekstosenseService;
import com.ey.resumeparser.utils.CommonUtils;
import com.tekstosense.segmenter.LearningType;
import com.tekstosense.segmenter.api.PdfSegmenterApi;

@Component("tekstosenseService")
public class TekstosenseServiceImpl implements TekstosenseService {

	@Value("${grobid_example.pGrobidHome}")
	private String grobidHome;

	@Override
	public Candidate parseResume(File file) {
		
		String pdfData = null;
		String name = null;
		//ClassLoader classLoader = new TekstosenseServiceImpl().getClass().getClassLoader();
		//PdfSegmenterApi p = new PdfSegmenterApi(classLoader.getResource(grobidHome).getPath().toString(), classLoader.getResource(grobidHome).getPath().toString() + "/config/grobid.properties");
		PdfSegmenterApi p = new PdfSegmenterApi(grobidHome,  grobidHome + "/config/grobid.properties");
		try {
			pdfData = p.getPdfSegments(file, LearningType.SUPERVISED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(pdfData));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("name");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				name = element.getTextContent();
			}
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Candidate candidate = new Candidate();
		candidate.setName(name);
		candidate.setEmail(CommonUtils.fetchEmail(pdfData));
		candidate.setPhoneNumber(CommonUtils.fetchPhoneNumber(pdfData));
		return candidate;
	}

}
