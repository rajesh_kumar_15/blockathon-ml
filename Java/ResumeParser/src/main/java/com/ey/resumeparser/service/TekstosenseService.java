package com.ey.resumeparser.service;

import java.io.File;

import com.ey.resumeparser.models.Candidate;

public interface TekstosenseService {
	
	public Candidate parseResume(File file);

}
