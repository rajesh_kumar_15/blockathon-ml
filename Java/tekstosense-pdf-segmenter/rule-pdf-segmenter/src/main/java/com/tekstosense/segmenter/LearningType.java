package com.tekstosense.segmenter;

/**
 * Created by abhishekpradhan on 5/5/17.
 */
public enum LearningType {
    RULE_BASED, SUPERVISED
}
