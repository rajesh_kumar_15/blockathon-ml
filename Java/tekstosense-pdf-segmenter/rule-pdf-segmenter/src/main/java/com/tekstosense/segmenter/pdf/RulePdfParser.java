package com.tekstosense.segmenter.pdf;


import com.google.common.base.Preconditions;
import com.tekstosense.segmenter.RulePdfSegmenter;
import com.tekstosense.segmenter.supervised.pdf.PdfParser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RulePdfParser implements PdfParser {

    RulePdfSegmenter pdfSegmenter = new RulePdfSegmenter();

    @Override
    public List<String> parsePdf(List<File> files) throws Exception {

        List<String> tei = new ArrayList<>();
        for(File pdfPath : Preconditions.checkNotNull(files,"Files can not be null")) {
            String pdfSegment = pdfSegmenter.getSegments(pdfPath);
            tei.add(pdfSegment);
        }
        return tei;
    }

    @Override
    public String parsePdf(File file) throws Exception {
        List<File> files = Collections.singletonList(Preconditions.checkNotNull(file,"File can not be null"));
        List<String> tei = parsePdf(files);
        Preconditions.checkElementIndex(0,tei.size());
        return tei.get(0);
    }
}
