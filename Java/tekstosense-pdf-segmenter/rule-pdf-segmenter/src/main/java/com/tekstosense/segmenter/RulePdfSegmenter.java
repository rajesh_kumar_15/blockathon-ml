package com.tekstosense.segmenter;

import com.google.common.base.Preconditions;
import com.tekstosense.segmenter.rulebased.StructurePdf.PdfSections;
import com.tekstosense.segmenter.rulebased.model.Section;
import com.tekstosense.segmenter.formatter.TekstosenseTEIFormater;
import com.tekstosense.segmenter.supervised.models.NamedEntity;
import com.tekstosense.segmenter.supervised.parser.NamedEntityParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.grobid.core.document.Document;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by abhishekpradhan on 5/5/17.
 */
public class RulePdfSegmenter implements PDFSegmenter {

    private PdfSections pdfSections;

    public RulePdfSegmenter() {
        this.pdfSections = new PdfSections();
    }

    @Override
    public String getSegments(File file) throws IOException {
        List<File> files = Collections.singletonList(Preconditions.checkNotNull(file, "File can not be NUll"));
        List<String> sections = processPdf(files);
        if (sections != null && !sections.isEmpty()) {
            return sections.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<String> getSegments(List<File> files) throws IOException {
        return processPdf(Preconditions.checkNotNull(files, "Files can not be null"));
    }

    private String extractText(File file) throws IOException {
        PDDocument doc = PDDocument.load(file);
        return new PDFTextStripper().getText(doc);
    }


    private List<String> processPdf(List<File> files) throws IOException {
        List<String> processedFiles = new ArrayList<>();
        this.pdfSections.processFile(files);
        Map<File, List<Section>> filesSectioned = this.pdfSections.getStructuredPdf();
        for (Map.Entry<File, List<Section>> file : filesSectioned.entrySet()) {

            // Extracting text and sections using rule-based approach
            PdfFile pdfFile = new PdfFile(extractText(file.getKey()), file.getValue());

            // Custom parsers applied to the PDF files
            NamedEntityParser namedEntityParser = new NamedEntityParser();
            List<NamedEntity> namedEntities = namedEntityParser.extractNamedEntities(pdfFile.getText());

            pdfFile.setNamedEntities(namedEntities);

            //Converting the extracted information to TEI format
            processedFiles.add(toTEI(pdfFile));
        }
        return processedFiles;
    }

    private String toTEI(PdfFile file) {
        Document doc = Document.createFromText(file.getText());
        TekstosenseTEIFormater teiFormatter = new TekstosenseTEIFormater();
        if (doc.getBlocks() != null) {

            //Edit below line to include abstract if found
            StringBuilder tei = teiFormatter.toTEIHeader(file);
            tei = teiFormatter.toTEIBody(tei, file.getSections());
            tei = teiFormatter.toTEINamedEntity(tei, file.getNamedEntities());

            // End body section before Back section begins
            tei.append("\n\t\t</body>");
            tei = teiFormatter.toTEIBack(tei);

            // Terminate the text and TEI section.
            tei.append("\n\t</text>\n");
            tei.append("\n</TEI>\n");

            doc.setTei(tei.toString());
            return doc.getTei();
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        RulePdfSegmenter pdfSegmenter = new RulePdfSegmenter();
        String pdfSegment = pdfSegmenter.getSegments(new File("/Users/abhishekpradhan/Dropbox/Automatic Keyword Extraction/HUMB- Automatic Key Term Extraction from Scientific Articles in GROBID.pdf"));
        System.out.println(pdfSegment);
    }
}
