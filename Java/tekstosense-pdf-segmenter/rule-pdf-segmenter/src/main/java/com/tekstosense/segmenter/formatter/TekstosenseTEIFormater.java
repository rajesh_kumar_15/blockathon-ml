package com.tekstosense.segmenter.formatter;

import com.google.common.base.Preconditions;
import com.tekstosense.segmenter.PdfFile;
import com.tekstosense.segmenter.rulebased.model.Section;
import com.tekstosense.segmenter.rulebased.model.Structure;
import com.tekstosense.segmenter.rulebased.util.StructureUtil;
import com.tekstosense.segmenter.supervised.models.NamedEntity;
import org.grobid.core.document.Document;
import org.grobid.core.document.TEIFormatter;
import org.grobid.core.engines.config.GrobidAnalysisConfig;

import java.util.List;

/**
 * Created by abhishekpradhan on 6/5/17.
 */
public class TekstosenseTEIFormater {


    public StringBuilder toTEIHeader(PdfFile pdfFile){
        Document doc = Document.createFromText(pdfFile.getText());
        doc.setLanguage("en");
        TEIFormatter teiFormatter = new TEIFormatter(doc);
        return teiFormatter.toTEIHeader(null, (String)null, GrobidAnalysisConfig.defaultInstance());
    }

    // Must be edited when reference extraction is completed in rule-based algo.
    public StringBuilder toTEIBack(StringBuilder tei){
        tei.append("\n\t\t<back/>");
        return tei;
    }


    public StringBuilder toTEINamedEntity(StringBuilder tei, List<NamedEntity> nes) {
        if (nes != null && !nes.isEmpty()) {
            for (NamedEntity namedEntity : nes) {
                String neString = getNeTeiString(namedEntity);
                tei.append("\t\t" + neString);
            }
        }
        return tei;
    }

    /*
        Named Entity TEI format inspiration from http://tei.it.ox.ac.uk/Talks/2010-07-oxford/talk-04-names.pdf
     */
    private String getNeTeiString(NamedEntity namedEntity) {
        StringBuilder neString = new StringBuilder();
        neString.append("\t\t<name type=\""+namedEntity.getEntityType().replaceAll("\n","")+"\">");
        neString.append(namedEntity.getEntityName().trim());
        neString.append("</name>\n");
        return neString.toString();
    }

    public StringBuilder toTEIBody(StringBuilder buffer, List<Section> sections) {
        List<Structure> structures = StructureUtil.toStructure(Preconditions.checkNotNull(sections, "Sections can not be null"));
        buffer.append("\t\t<body>\n");
        for (int i = 0; i < structures.size(); i++) {
            Structure structure = structures.get(i);
            String teiSection = buildTEISection(i, structure);
            buffer.append(teiSection);
        }
        return buffer;
    }

    private String buildTEISection(int id, Structure section) {
        StringBuilder sectionString = new StringBuilder();
        sectionString.append("\t\t\t<div xmlns=\"http://www.tei-c.org/ns/1.0\">\n");
        sectionString.append("\t\t\t\t<head n=\"" + id + "\">" + section.getHeading() + "</head>\n");
        sectionString.append("\t\t\t\t<p>");
        List<String> lines = section.getText();
        for (String line : lines) {
            sectionString.append(line.trim().replaceAll("\n","")+" ");
        }
        sectionString.append("</p>");
        sectionString.append("\n\t\t\t</div>\n");
        return sectionString.toString();
    }
}
