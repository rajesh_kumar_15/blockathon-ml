package com.tekstosense.segmenter.api;

import com.tekstosense.segmenter.LearningType;
import com.tekstosense.segmenter.pdf.RulePdfParser;
import com.tekstosense.segmenter.supervised.config.TekstosenseConfig;
import com.tekstosense.segmenter.supervised.pdf.PdfParser;
import com.tekstosense.segmenter.supervised.pdf.SupervisedPdfParser;

import java.io.File;
import java.util.List;

import static com.tekstosense.segmenter.LearningType.SUPERVISED;

/**
 * Created by abhishekpradhan on 12/5/17.
 */
public class PdfSegmenterApi {

    public PdfSegmenterApi(String home, String prop) {
        //String home = "C:/Users/pankaja.patel/Desktop/blockathon/kermitt2-grobid-aa8652b/grobid-home";
        //String prop = "C:/Users/pankaja.patel/Desktop/blockathon/kermitt2-grobid-aa8652b/grobid-home/config/grobid.properties";
        TekstosenseConfig.setTekstosenseConfig(home, prop);
    }

    public String getPdfSegments(File pdf, LearningType type) throws Exception {

        if (type.equals(LearningType.RULE_BASED)) {
            PdfParser pdfParser = new RulePdfParser();
            return pdfParser.parsePdf(pdf);
        } else if (type.equals(SUPERVISED)) {
            PdfParser pdfParser = new SupervisedPdfParser();
            return pdfParser.parsePdf(pdf);
        } else {
            throw new IllegalArgumentException(" Illegal Learning Type. ");
        }
    }

    public List<String> getPdfSegments(List<File> pdf, LearningType type) throws Exception {
        if (type.equals(LearningType.RULE_BASED)) {
            PdfParser pdfParser = new RulePdfParser();
            return pdfParser.parsePdf(pdf);
        } else if (type.equals(SUPERVISED)) {
            PdfParser pdfParser = new SupervisedPdfParser();
            return pdfParser.parsePdf(pdf);
        } else {
            throw new IllegalArgumentException(" Illegal Learning Type. ");
        }
    }

    public static void main(String[] args) throws Exception {
        File file = new File("C:/Users/pankaja.patel/Desktop/blockathon/Java/resumes/test/AravindSubbarao_CV.pdf");
//        File file = new File("/Users/abhishekpradhan/Desktop/demo/resume-samples.pdf");
        String home = "C:/Users/pankaja.patel/Desktop/blockathon/kermitt2-grobid-aa8652b/grobid-home";
        String prop = "C:/Users/pankaja.patel/Desktop/blockathon/kermitt2-grobid-aa8652b/grobid-home/config/grobid.properties";
        PdfSegmenterApi segmenterApi = new PdfSegmenterApi(home,prop);
        try {
        System.out.println(segmenterApi.getPdfSegments(file, LearningType.SUPERVISED));
        }catch ( Exception ex) {
        	System.out.println("In catch block");
        	 System.out.println(segmenterApi.getPdfSegments(file, LearningType.RULE_BASED));
        }
    }
}
