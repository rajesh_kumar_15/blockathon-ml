package com.tekstosense.segmenter;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by abhishekpradhan on 5/5/17.
 */
public interface PDFSegmenter {

    String getSegments(File file) throws IOException;
    List<String> getSegments(List<File> files) throws IOException;
}
