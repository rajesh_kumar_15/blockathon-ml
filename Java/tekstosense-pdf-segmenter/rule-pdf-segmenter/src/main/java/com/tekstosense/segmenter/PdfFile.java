package com.tekstosense.segmenter;

import com.tekstosense.segmenter.rulebased.model.Section;
import com.tekstosense.segmenter.supervised.models.NamedEntity;

import java.util.List;

/**
 * Created by abhishekpradhan on 6/5/17.
 */
public class PdfFile {

    private String text;
    private List<Section> sections;
    private List<NamedEntity> namedEntities;

    public PdfFile(String text, List<Section> sections) {
        this.text = text;
        this.sections = sections;
    }

    public PdfFile() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<NamedEntity> getNamedEntities() {
        return namedEntities;
    }

    public void setNamedEntities(List<NamedEntity> namedEntities) {
        this.namedEntities = namedEntities;
    }
}
