package com.tekstosense.segmenter.supervised.parser;


import com.tekstosense.segmenter.supervised.config.TekstosenseConfig;
import com.tekstosense.segmenter.supervised.models.NamedEntity;
import org.grobid.core.data.Entity;
import org.grobid.core.engines.NEREnParser;
import org.grobid.core.engines.NERParser;
import org.grobid.core.utilities.GrobidProperties;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by abhishekpradhan on 1/5/17.
 */
public class NamedEntityParser{

    private NERParser nerParser;

    public NamedEntityParser() {
        nerParser = new NEREnParser();
    }

    public List<NamedEntity> extractNamedEntities(String text) {

        List<Entity> results = this.nerParser.extractNE(text);
        return results.stream().map(e -> new NamedEntity(e.getRawName(), e.getType().getName())).collect(Collectors.toList());
    }

}
