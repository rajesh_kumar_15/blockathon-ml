package com.tekstosense.segmenter.supervised.pdf;

import java.io.File;
import java.util.List;

/**
 * Created by abhishekpradhan on 12/5/17.
 */
public interface PdfParser {

    List<String> parsePdf(List<File> files) throws Exception;
    String parsePdf(File file) throws Exception;
}
