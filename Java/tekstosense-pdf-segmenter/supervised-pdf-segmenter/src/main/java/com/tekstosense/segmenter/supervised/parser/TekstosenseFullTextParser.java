package com.tekstosense.segmenter.supervised.parser;

import com.tekstosense.segmenter.supervised.models.NamedEntity;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.grobid.core.data.*;
import org.grobid.core.document.Document;
import org.grobid.core.document.DocumentPiece;
import org.grobid.core.document.TEIFormatter;
import org.grobid.core.engines.EngineParsers;
import org.grobid.core.engines.FullTextParser;
import org.grobid.core.engines.SegmentationLabel;
import org.grobid.core.engines.config.GrobidAnalysisConfig;
import org.grobid.core.exceptions.GrobidException;
import org.grobid.core.layout.LayoutToken;
import org.grobid.core.layout.LayoutTokenization;
import org.grobid.core.utilities.Pair;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by abhishekpradhan on 12/5/17.
 */
public class TekstosenseFullTextParser extends FullTextParser {

    /**
     * TODO some documentation...
     *
     * @param parsers
     */
    public TekstosenseFullTextParser(EngineParsers parsers) {
        super(parsers);
    }




    /**
     * Create the TEI representation for a document based on the parsed header, references
     * and body sections.
     */
    @Override
    public void toTEI(Document doc,
                      String reseBody,
                      String reseAnnex,
                      LayoutTokenization layoutTokenization,
                      List<LayoutToken> tokenizationsAnnex,
                      BiblioItem resHeader,
                      List<BibDataSet> resCitations,
                      List<Figure> figures,
                      List<Table> tables,
                      List<Equation> equations,
                      GrobidAnalysisConfig config) {
        if (doc.getBlocks() == null) {
            return;
        }
        TEIFormatter teiFormatter = new TEIFormatter(doc);
        StringBuilder tei;
        try {
            tei = teiFormatter.toTEIHeader(resHeader, null, config);

            //System.out.println(rese);
            //int mode = config.getFulltextProcessingMode();
            tei = teiFormatter.toTEIBody(tei, reseBody, resHeader, resCitations,
                    layoutTokenization, figures, tables, equations, doc, config);

            NamedEntityParser namedEntityParser = new NamedEntityParser();
            List<NamedEntity> namedEntities = namedEntityParser.extractNamedEntities(doc.getAllBlocksClean(0, 1));
            tei = toTEINamedEntity(tei,namedEntities);

            tei.append("\t\t<back>\n");

            // acknowledgement is in the back
            SortedSet<DocumentPiece> documentAcknowledgementParts =
                    doc.getDocumentPart(SegmentationLabel.ACKNOWLEDGEMENT);
            Pair<String, LayoutTokenization> featSeg =
                    getBodyTextFeatured(doc, documentAcknowledgementParts);
            List<LayoutToken> tokenizationsAcknowledgement;
            if (featSeg != null) {
                // if featSeg is null, it usually means that no body segment is found in the
                // document segmentation
                String acknowledgementText = featSeg.getA();
                tokenizationsAcknowledgement = featSeg.getB().getTokenization();
                String reseAcknowledgement = null;
                if ( (acknowledgementText != null) && (acknowledgementText.length() >0) )
                    reseAcknowledgement = label(acknowledgementText);
                tei = teiFormatter.toTEIAcknowledgement(tei, reseAcknowledgement,
                        tokenizationsAcknowledgement, resCitations, config);
            }

            tei = teiFormatter.toTEIAnnex(tei, reseAnnex, resHeader, resCitations,
                    tokenizationsAnnex, doc, config);

            tei = teiFormatter.toTEIReferences(tei, resCitations, config);
            doc.calculateTeiIdToBibDataSets();

            tei.append("\t\t</back>\n");

            tei.append("\t</text>\n");
            tei.append("</TEI>\n");
        } catch (Exception e) {
            throw new GrobidException("An exception occurred while running Grobid.", e);
        }
        doc.setTei(tei.toString());

        //TODO: reevaluate
//		doc.setTei(
//				XmlBuilderUtils.toPrettyXml(
//						XmlBuilderUtils.fromString(tei.toString())
//				)
//		);
    }


    public StringBuilder toTEINamedEntity(StringBuilder tei, List<NamedEntity> nes) {
        if (nes != null && !nes.isEmpty()) {
            for (NamedEntity namedEntity : nes) {
                String neString = getNeTeiString(namedEntity);
                tei.append("\t\t" + neString);
            }
        }
        return tei;
    }

    private String extractText(File file) throws IOException {
        PDDocument doc = PDDocument.load(file);
        return new PDFTextStripper().getText(doc);
    }

    /*
        Named Entity TEI format inspiration from http://tei.it.ox.ac.uk/Talks/2010-07-oxford/talk-04-names.pdf
     */
    private String getNeTeiString(NamedEntity namedEntity) {
        StringBuilder neString = new StringBuilder();
        neString.append("\t\t<name type=\""+namedEntity.getEntityType().replaceAll("\n","")+"\">");
        neString.append(namedEntity.getEntityName().trim());
        neString.append("</name>\n");
        return neString.toString();
    }
}
