package com.tekstosense.segmenter.supervised.util;

import java.util.Arrays;

/**
 * Created by abhishekpradhan on 1/5/17.
 */
public enum TekstosenseMEEngine {

    MaximumEntropy("bin");



    private final String ext;

    TekstosenseMEEngine(String ext) {
        this.ext = ext;
    }

    public String getExt() {
        return ext;
    }

    public static TekstosenseMEEngine get(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name of a engine must not be null");
        }

        String n = name.toLowerCase();
        for (TekstosenseMEEngine e : values()) {
            if (e.name().toLowerCase().equals(n)) {
                return e;
            }
        }
        throw new IllegalArgumentException("No engine with name '" + name +
                "', possible values are: " + Arrays.toString(values()));
    }

    public static void main(String[] args) {
        TekstosenseMEEngine engine = TekstosenseMEEngine.MaximumEntropy;
        System.out.println(engine.getExt());
    }
}

