package com.tekstosense.segmenter.supervised.parser;

import org.grobid.core.engines.EngineParsers;

/**
 * Created by abhishekpradhan on 1/5/17.
 */
public class TekstosenseEngineParsers extends EngineParsers {

    private NamedEntityParser namedEntityParser = null;
    private TekstosenseFullTextParser tekstosenseFullTextParser;

    public NamedEntityParser getNamedEntityParser() {
        if (namedEntityParser == null) {
            synchronized (this) {
                if (namedEntityParser == null) {
                    namedEntityParser = new NamedEntityParser();
                }
            }
        }
        return namedEntityParser;
    }

    public TekstosenseFullTextParser getTekstosenseFullTextParser() {
        if (tekstosenseFullTextParser == null) {
            synchronized (this) {
                if (tekstosenseFullTextParser == null) {
                    tekstosenseFullTextParser = new TekstosenseFullTextParser(this);
                }
            }
        }
        return tekstosenseFullTextParser;
    }
}
