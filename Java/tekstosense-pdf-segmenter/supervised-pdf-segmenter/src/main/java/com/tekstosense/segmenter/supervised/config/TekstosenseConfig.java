package com.tekstosense.segmenter.supervised.config;

import org.grobid.core.main.LibraryLoader;
import org.grobid.core.mock.MockContext;
import org.grobid.core.utilities.GrobidProperties;

/**
 * Created by abhishekpradhan on 12/5/17.
 */
public class TekstosenseConfig {

    public static void setTekstosenseConfig(String homeFolder,String propertiesFile){
        try {
            MockContext.setInitialContext(homeFolder, propertiesFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GrobidProperties.getInstance();
        LibraryLoader.load();
    }
}
