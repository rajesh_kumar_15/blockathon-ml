package com.tekstosense.segmenter.supervised.util;

import org.grobid.core.GrobidModel;
import org.grobid.core.engines.tagging.GrobidCRFEngine;
import org.grobid.core.utilities.GrobidProperties;

import java.io.File;

/**
 * Created by abhishekpradhan on 1/5/17.
 */
public class TekstosenseProperties extends GrobidProperties {

    /**
     * Type of CRF framework used
     */
    private static TekstosenseMEEngine engine = TekstosenseMEEngine.MaximumEntropy;

    public static File getMEModelPath(final GrobidModel model) {
        return new File(get_GROBID_HOME_PATH(), FOLDER_NAME_MODELS + File.separator
                + model.getFolderName() + File.separator
                + FILE_NAME_MODEL + "." + engine.getExt());
    }

    public static File getGrobidModelPath(final GrobidModel model) {
        return new File(get_GROBID_HOME_PATH(), FOLDER_NAME_MODELS + File.separator
                + model.getFolderName() + File.separator
                + FILE_NAME_MODEL + "." + GrobidCRFEngine.WAPITI.getExt());
    }
}
