package com.tekstosense.segmenter.supervised.config;

import com.tekstosense.segmenter.supervised.util.TekstosenseMEEngine;
import org.grobid.core.engines.tagging.GrobidCRFEngine;

/**
 * Created by abhishekpradhan on 12/5/17.
 */
public class EngineConfig {

    public GrobidCRFEngine getGrobidWapitiEngine(){
        return GrobidCRFEngine.WAPITI;
    }

    public GrobidCRFEngine getGrobidCrfppEngine(){
        return GrobidCRFEngine.CRFPP;
    }

    public TekstosenseMEEngine getTekstosenseMEEngine(){
        return TekstosenseMEEngine.MaximumEntropy;
    }
}
