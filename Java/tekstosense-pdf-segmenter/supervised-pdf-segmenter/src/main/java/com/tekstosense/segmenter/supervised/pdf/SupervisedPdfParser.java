package com.tekstosense.segmenter.supervised.pdf;


import com.google.common.base.Preconditions;
import org.grobid.core.document.Document;
import org.grobid.core.engines.Engine;
import org.grobid.core.engines.config.GrobidAnalysisConfig;
import org.grobid.core.factory.GrobidFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SupervisedPdfParser implements PdfParser{

    private Engine engine = GrobidFactory.getInstance().createEngine();
    private GrobidAnalysisConfig config = GrobidAnalysisConfig.defaultInstance();

    @Override
    public List<String> parsePdf(List<File> files) throws Exception {

        List<String> tei = new ArrayList<>();
        for(File pdfPath : Preconditions.checkNotNull(files,"Files can not be null")) {
            Document teiDocument = engine.tekstosenseFullTextToTEIDoc(pdfPath, config);
            tei.add(teiDocument.getTei());
        }
        return tei;
    }

    @Override
    public String parsePdf(File file) throws Exception {
        List<File> files = Collections.singletonList(Preconditions.checkNotNull(file,"File can not be null"));
        List<String> tei = parsePdf(files);
        Preconditions.checkElementIndex(0,tei.size());
        return tei.get(0);

    }
}
