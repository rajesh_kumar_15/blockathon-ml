package com.tekstosense.segmenter.supervised.models;

/**
 * Created by abhishekpradhan on 1/5/17.
 */
public class NamedEntity {

    private String entityName;
    private String entityType;

    public NamedEntity(String entityName, String entityType) {
        this.entityName = entityName;
        this.entityType = entityType;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }
}
